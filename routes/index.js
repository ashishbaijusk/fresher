var express = require('express');
var router = express.Router();
var fresher=require('../models/fresher');
var District=require('../models/district');
var College=require('../models/college');

router.get('/', (req,res,next)=>{
    res.redirect('https://fierce-taiga-78346.herokuapp.com/freshers_original.html');
});

router.post('/add',(req,res,next)=>{
  newfresher  = new fresher({
    student_name:req.body.student_name,
    parents_name:req.body.parents_name,
    email:req.body.email,
    mobile:req.body.mobile,
    gender:req.body.gender,
    nationality:req.body.nationality,
    roll_no:req.body.roll_no,
    pincode:req.body.pincode,
    state:req.body.state,
    pu_state:req.body.pu_state,
    pu_college:req.body.pu_college,
    percentage:req.body.percentage,
    pu_district:req.body.pu_district,
    ans1:req.body.ans1,
    ans2:req.body.ans2
  });
  fresher.addFresher(newfresher,(err,fresher)=>{
      if(err)
      {
        throw err;
      }
      else {
        pu_district=req.body.pu_district;
    District.findOne({pu_district:pu_district},(err,fresher)=>{
    if(err)
    {
      throw err;
    }
    else {
      if(!fresher){
        newdistrict=   new District({
          pu_district:newfresher.pu_district,
          count:"1"
        });
        District.addDistrict(newdistrict,(err,dist)=>{
          if(err)
          {
            throw err;
          }
          else {
            pu_college=req.body.pu_college;
        College.findOne({pu_college:pu_college},(err,fresher)=>{
        if(err)
        {
          throw err;
        }
        else {
          if(!fresher){
            newcollege=   new College({
              pu_college:pu_college,
              count:"1"
            });
            College.addCollege(newcollege,(err,college)=>{
              if(err)
              {
                throw err;
              }
            });
          }
          else {
            id=fresher._id;
            count=fresher.count+1;
            College.findByIdAndUpdate(id,{count:count},(err,college)=>{
              if(err){
                throw err;
              }
              else {
                res.json({success:true,msg:"added"});
              }
            })
          }
        }
      })
          }
        });
      }
      else {
        id=fresher._id;
        count=fresher.count+1;
        District.findByIdAndUpdate(id,{count:count},(err,dist)=>{
          if(err){
            throw err;
          }
          else {
            pu_college=req.body.pu_college;
        College.findOne({pu_college:pu_college},(err,fresher)=>{
        if(err)
        {
          throw err;
        }
        else {
          if(!fresher){
            newcollege=   new College({
              pu_college:pu_college,
              count:"1"
            });
            College.addCollege(newcollege,(err,college)=>{
              if(err)
              {
                throw err;
              }
            });
          }
          else {
            id=fresher._id;
            count=fresher.count+1;
            College.findByIdAndUpdate(id,{count:count},(err,college)=>{
              if(err){
                throw err;
              }
              else {
                res.json({success:true,msg:"added"});
              }
            })
          }
        }
      })
      }
        })
      }
    }
  })
  }
  });
});

router.get('/show',(req,res,next)=>{
  District.find({},(err,dist)=>{
    if(err){
      throw err;
    }
    else{
      College.find({}, (err, college)=>{
        if(err)
          throw err;
        else
        res.render("show",{dist:dist, college:college});
      }).sort({count:1});
    }
  }).sort({count:1});
});


router.get('/showrollno',(req,res,next)=>{
  fresher.find({},(err,fresher)=>{
    if(err)
    throw err;
    else{
      res.render("rollnumber",{fresher:fresher});
    }
  }).sort({roll_no:1});
})



module.exports = router;
