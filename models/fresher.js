const mongoose  = require('mongoose');

const Schema  = mongoose.Schema;

mongoose.Promise  = global.Promise;


const fresherschema = new Schema({
                      student_name:String,
                      parents_name:String,
                      email:String,
                      mobile:Number,
                      gender:String,
                      nationality:String,
                      roll_no:String,
                      pincode:String,
                      state:String,
                      pu_college:String,
                      percentage:Number,
                      pu_state:String,
                      pu_district:String,
                      ans1: String,
                      ans2:String
});
const Fresher = module.exports  = mongoose.model("Fresher",fresherschema);

module.exports.addFresher = (newfresher,callback)=>{
  Fresher.create(newfresher,callback);
}
