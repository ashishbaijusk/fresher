const mongoose  = require('mongoose');

const Schema  = mongoose.Schema;

mongoose.Promise  = global.Promise;


const districtschema = new Schema({
                      pu_district:String,
                      count:Number

});
const District = module.exports  = mongoose.model("District",districtschema);

module.exports.addDistrict = (newdistrict,callback)=>{
  District.create(newdistrict,callback);
}
