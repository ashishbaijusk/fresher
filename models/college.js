const mongoose  = require('mongoose');

const Schema  = mongoose.Schema;

mongoose.Promise  = global.Promise;


const collegeschema = new Schema({
                      pu_college:String,
                      count:Number

});
const College = module.exports  = mongoose.model("College",collegeschema);

module.exports.addCollege = (newcollege,callback)=>{
  College.create(newcollege,callback);
}
